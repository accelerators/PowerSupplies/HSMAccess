# HSMAccess

Tango classes to manage:
1. The EBS Comeca PowerSupply channels
2. The HotSwapManager (HSM) box 

It contains four Tango classes which are:

* **HSMAccess** for the control of the HSM box
* **HSMPowerSupply** Power supply control on top of ComecaChannel (It handles HSM and/or Statum modbus communication)
* **ComecaChannel** Interface to Comeca Statum channel (modbus)
* **Modbus** The classical ESRF Tango classes

## Building and Installation

### Dependencies

The project has the following dependencies.

#### Project Dependencies

* Tango Controls 9 or higher.
* The ESRF ISDD electronic group **libdeep** library. This library is used to
communicate with the HSM box.

#### Toolchain Dependencies

* C++11 compliant compiler.
* CMake 3.0 or greater is required to perform the build.

### Build

How to build HSMAccess:

```bash
git clone https://gitlab.esrf.fr/accelerators/PowerSupplies/HSMAccess.git
cd HSMAccess
mkdir build
cd build
cmake ..
make
```

How to build ComecaChannel:

```bash
git clone https://gitlab.esrf.fr/accelerators/PowerSupplies/HSMAccess.git
cd HSMAccess/ComecaChannel
mkdir build
cd build
cmake ..
make
```

Here is a list of usefull `<CMAKE_OPTIONS>`: 

| BUILD FLAG         | DESCRIPTION                 | VALUES                                       | DEFAULT                                 |
|--------------------|-----------------------------|----------------------------------------------|-----------------------------------------|
| `CMAKE_BUILD_TYPE` | Specify the build type      | Debug, Release, RelWithDebInfo or MinSizeRel | Debug                                   |
| `WITH_LEBDEEP`     | Location of the libdeep dir |                                              | $ENV{LIBDEEP_DIR}                       |
| `WITH_MODBUS`      | Location of the MODBUS dir  |                                              | /segfs/tango/cppserver/protocols/Modbus |


