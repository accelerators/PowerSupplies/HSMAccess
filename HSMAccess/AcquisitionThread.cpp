//=============================================================================
//
// file :        AcquisitionThread.cpp
//
// description : C++ source for the HSMAccess class acquisition thread.
//
// project :
//
// This file is part of Tango device class.
//
// Tango is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Tango is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Tango.  If not, see <http://www.gnu.org/licenses/>.
//
//
// Copyright (C): 2004,2005,2006,2007,2008,2009,2010,2011,2012,2013,2014,2015,2016,2017
//                European Synchrotron Radiation Facility
//                BP 220, Grenoble 38043
//                France
//=============================================================================
#include <tango.h>
#include "HSMAccess.h"

#include "hsm_parser.hpp"

namespace HSMAccess_ns {
    
void HSMAccess::start_acq_thread() {

  if(!acqThreadRunning) {
    cout << "HSMAccess AcquisitionThread starting..." << endl;
    exitAcqThread = false;
    acqThreadHandle = std::thread(&HSMAccess::acq_thread, this);
    
    cout << "HSMAccess AcquisitionThread started..." << endl;
  } else {
    cout << "HSMAccess already started..." << endl;
  }

}

void HSMAccess::stop_acq_thread() {
  exitAcqThread = true;
  if (acqThreadHandle.joinable())
    acqThreadHandle.join();
  acqThreadRunning = false;
}

void HSMAccess::acq_thread() {

  omni_thread::ensure_self es;

  int err_nb = 0;
  acqThreadRunning = true;

  // Thread loop
  while (!exitAcqThread) {
    if(!noHsm){
        state_.ClearAllErrors();

        string except_mess;
        try {
                // Get data from hardware
                //put call_state in first
                call_state();
                call_dcmag();
                call_magcurr();
                call_magcfg();
                call_adccfg();
                call_magset();
                call_dcset();
                call_mosspy();
                err_nb = 0;
                push_hsmdata_pipe_event();
        } catch (Tango::DevFailed &e) {
          except_mess = e.errors[0].desc.in();
          err_nb++;
        } catch (const std::exception &error) {
          except_mess = error.what();
          err_nb++;
        }

        if (err_nb >= 2) {
            try{
                get_dev_monitor().get_monitor();
                stringstream ss;
                ss << "Communication error with HSM: " << hsmNetName;
                ss << "\nError: " << except_mess;
                state_.NotifyError(ss.str(), 0);
                get_dev_monitor().rel_monitor();
            }catch(Tango::DevFailed &ex){
                cerr << device_name << ":" << ex.errors[0].desc << endl;
            }
        }
    }

    // 1 second loop
    int toSleep = 1000;
    while (toSleep > 0 && !exitAcqThread) {
      usleep(100000);
      toSleep -= 100;
    }

  } // end while(exit_th){}

  deepdev_freethread();
  
  cout << "HSMAccess AcquisitionThread exiting..." << endl;
}

//-------------------------------------------------------------------------------------------------------
// Call command MAGCFG ALL on HSM hardware and get interesting data from its result
// Used to init. THRES and TSETTLE vectors
//-------------------------------------------------------------------------------------------------------
void HSMAccess::call_magcfg() {

  string cmd = "?MAGCFG ALL";
  string data_received = query_hsm(cmd);
  std::unique_lock<std::mutex> guard(acqMutex);
  THRESH.assign(NB_POWERSUPPLY, NAN);
  TSETTLE.assign(NB_POWERSUPPLY, NAN);
  hsm::parse(cmd, data_received, NB_MAGNET, 7,
             [this](size_t idx, std::vector<std::string> &items) {
               size_t midx = std::find(CONNECTED_MAGNET.begin(),CONNECTED_MAGNET.end(),items[0]) - CONNECTED_MAGNET.begin();
               THRESH[midx] = stod(items[2]);
               TSETTLE[midx] = hsm::parse_time(items[4]);
             });

}

//------------------------------------------------------------------------------------------------------
// Call command ADCCFG ALL on HSM hardware and get interesting data from its result
// Used to init. NSAMPL vector
//-------------------------------------------------------------------------------------------------------
void HSMAccess::call_adccfg() {

  string cmd = "?ADCCFG ALL";
  string data_received = query_hsm(cmd);
  std::unique_lock<std::mutex> guard(acqMutex);
  hsm::parse(cmd, data_received, NB_MAGNET, 15,
             [this](size_t idx, std::vector<std::string> &items) {
               NSAMPL[idx] = stoi(items[4]);
               size_t midx = std::find(CONNECTED_MAGNET.begin(),CONNECTED_MAGNET.end(),items[0]) - CONNECTED_MAGNET.begin();
               GAIN[midx] = stod(items[12]);
               OFFSET[midx] = stod(items[14]);
             });

}

//+------------------------------------------------------------------------------------------------------
// Call command MAGSET ALL on HSM hardware and get interesting data from its result
// Used to init. CURR, VOLT and SWAP_INHIB vector
//-------------------------------------------------------------------------------------------------------
void HSMAccess::call_magset() {

  string cmd = "?MAGSET ALL";
  string data_received = query_hsm(cmd);
  std::unique_lock<std::mutex> guard(acqMutex);
  SWAP_INHIB.assign(NB_POWERSUPPLY, "YES");
  hsm::parse(cmd, data_received, NB_MAGNET, 10,
             [this](size_t idx, std::vector<std::string> &items) {
               CURR[idx] = (items[3] == "NONE") ? 0.0 : stod(items[3]);
               VOLT[idx] = (items[5] == "NONE") ? 0.0 : stod(items[5]);
               size_t midx = std::find(CONNECTED_MAGNET.begin(),CONNECTED_MAGNET.end(),items[0]) - CONNECTED_MAGNET.begin();
               SWAP_INHIB[midx] = items[9];
             });

}

//+------------------------------------------------------------------------------------------------------
// Call command DCSET ALL on HSM hardware and get interesting data from its result
// Used to init. CURR, VOLT for spare DC converters
//-------------------------------------------------------------------------------------------------------

void HSMAccess::call_dcset() {

  string cmd = "?DCSET ALL";
  string data_received = query_hsm(cmd);
  std::unique_lock<std::mutex> guard(acqMutex);
  hsm::parse(cmd, data_received, NB_POWERSUPPLY, 7,
             [this](size_t idx, std::vector<std::string> &items) {
               if (idx >= NB_MAGNET) {
                 CURR[idx] = (items[2] == "NONE") ? 0.0 : stod(items[2]);
                 VOLT[idx] = (items[4] == "NONE") ? 0.0 : stod(items[4]);
                 HSM_INHIB[idx] = items[6];
               }
             });

}

//------------------------------------------------------------------------------------------------------
// Call command DCMAG ALL on HSM hardware and get interesting data from its result
// Used to init. CONNECTED_MAGNET vector
//-------------------------------------------------------------------------------------------------------
void HSMAccess::call_dcmag() {

  string cmd = "?DCMAG ALL";
  string data_received = query_hsm(cmd);
  std::unique_lock<std::mutex> guard(acqMutex);
  hsm::parse(cmd, data_received, NB_POWERSUPPLY, 3,
             [this](size_t idx, std::vector<std::string> &items) {
               CONNECTED_MAGNET[idx] = items[1];
             });

}

//------------------------------------------------------------------------------------------------------
// Call command MAGCURR ALL on HSM hardware and get HSM current values
//-------------------------------------------------------------------------------------------------------
void HSMAccess::call_magcurr() {

  string cmd = "?MAGCURR ALL";
  std::string data_received = query_hsm(cmd);
  std::unique_lock<std::mutex> guard(acqMutex);
  HSM_CURRENT.assign(NB_POWERSUPPLY,NAN);
  hsm::parse(cmd, data_received, NB_MAGNET, 2,
                     [this,cmd,data_received](size_t idx, std::vector<std::string> &items) {
                       size_t midx = std::find(CONNECTED_MAGNET.begin(),CONNECTED_MAGNET.end(),items[0]) - CONNECTED_MAGNET.begin();
                       HSM_CURRENT[midx] = stod(items[1]);
                       if(fabs(HSM_CURRENT[midx])>=150.0) {
                         // Unexpected values for HSM current, log event
                         time_t now = time(nullptr);
                         cerr << "Unexpected hsmCurrent=" << HSM_CURRENT[midx] << " for " << CONNECTED_MAGNET[midx] << " at " << ctime(&now);
                         cerr << "HSM Command: " << cmd << endl;
                         cerr << "HSM Result: \n" << data_received << endl;
                       }
                     });

}

//-------------------------------------------------------------------------------------------------------
// Get the MOSPLATE Manager loop status
//-------------------------------------------------------------------------------------------------------
void HSMAccess::call_mosspy() {

  std::string data_received = query_hsm("?MOSSPY STATUS");
  std::unique_lock<std::mutex> guard(acqMutex);
  mosspy_on = data_received.find("ON") != string::npos;

}

//-------------------------------------------------------------------------------------------------------
// Get the HSM box main state
//-------------------------------------------------------------------------------------------------------
void HSMAccess::call_state() {
  std::string hsm_state_received;
  std::string mos_state_received;

  hsm_state_received = query_hsm("?STATE");
  try{
    mos_state_received = query_hsm("?STATE MOS RAW");
    
    get_dev_monitor().rel_monitor();
    state_.NotifyHwHsmState(hsm_state_received);
    state_.NotifyHwMosplateState(mos_state_received,soMode);
    get_dev_monitor().rel_monitor();
  }catch(Tango::DevFailed &ex){
    get_dev_monitor().get_monitor();
    state_.NotifyHwHsmState(hsm_state_received);
    get_dev_monitor().rel_monitor();
    throw ex;
  }
}


}   // end namespace
