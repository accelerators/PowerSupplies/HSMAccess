#pragma once

#include <algorithm>
#include <iterator>
#include <sstream>
#include <stdexcept>
#include <string>
#include <regex>

namespace hsm {

// Trim the given string
static std::string trim(const std::string &s)
{
  auto start = s.begin();
  while (start != s.end() && std::isspace(*start))
    start++;
  auto end = s.end();
  do {
    end--;
  } while (std::distance(start, end) > 0 && std::isspace(*end));

  return std::string{start, end + 1};
}

// Split given string into tokens using specified separator
static void split(std::vector<std::string> &tokens, const std::string &text, char sep) {

  size_t start = 0, end;
  tokens.clear();
  while ((end = text.find(sep, start)) != std::string::npos) {
    tokens.push_back(trim(text.substr(start, end - start)));
    start = end + 1;
  }
  tokens.push_back(trim(text.substr(start)));

}

// Replace all occurrence of from by to in str
static void replaceAll(std::string& str, const std::string& from, const std::string& to) {
  if(from.empty())
    return;
  size_t start_pos = 0;
  while((start_pos = str.find(from, start_pos)) != std::string::npos) {
    str.replace(start_pos, from.length(), to);
    start_pos += to.length(); // In case 'to' contains 'from', like replacing 'x' with 'yx'
  }
}

// Parse result of ?MODE command
template <class InputIt, class TertiaryFunction>
void parse_mode(InputIt first, InputIt last, TertiaryFunction handle) {

  // Initialized to PASSIVE ('P') just in case, but in theory ?MODE returns
  // every magnet type modes and therefore we should overwrite these default
  char q_mode = 'P', dq_mode = 'P', so_mode = 'P';

  for (auto line_begin = first, line_end = first; line_end != last;) {
    line_end = std::find(line_begin, last, '\n');

    auto mode_first_char = *line_begin;

    if (mode_first_char != 'P' and mode_first_char != 'M' and
        mode_first_char != 'H') {
      throw std::runtime_error("Expecting either PASSIVE, MASTER or HOTSWAP "
                               "string at the beginning of a line. Got \"" +
                               std::string(line_begin, line_end) + "\".");
    }

    for (auto mag_type_begin = std::find(line_begin, line_end, ' ');
         mag_type_begin != line_end;
         mag_type_begin = std::find(mag_type_begin, line_end, ' ')) {

      // Advance after the whitespace
      ++mag_type_begin;

      switch (*mag_type_begin) {
      case 'A': // Mag type = ALL
        q_mode = dq_mode = so_mode = mode_first_char;
        line_end = last; // This break the for loop
        break;
      case 'D': // Mag type = DQMAGS
        dq_mode = mode_first_char;
        break;
      case 'Q': // Mag type = QMAGS
        q_mode = mode_first_char;
        break;
      case 'S': // Mag type = SOMAGS
        so_mode = mode_first_char;
        break;
      default:
        throw std::runtime_error(
            "Expecting either ALL, QMAGS, DQMAGS or SOMAGS "
            "string after the mode name. Got \"" +
            std::string(mag_type_begin, line_end) + "\".");
      }
    }
    line_begin = std::next(line_end);
  }

  handle(q_mode, dq_mode, so_mode);
}

// Parse result of ?XXXX ALL command
template <class TertiaryFunction>
void parse(std::string& cmd, std::string& data, size_t expectedNbLine, size_t expectedNbItems, TertiaryFunction handle) {

  std::vector<std::string> lines;
  split(lines,data,'\n');
  if(lines.size()!=expectedNbLine) {
    throw std::runtime_error(
            cmd + ": Unexpected number of PS, got " + std::to_string(lines.size()) + ", " +
            std::to_string(expectedNbLine) + " expected");
  }
  for(size_t i=0;i<lines.size();i++) {
    std::vector<std::string> items;
    split(items,lines[i],' ');
    if(items.size()<expectedNbItems) items.insert(items.end(),(expectedNbItems-items.size()),"");
    if(items.size()!=expectedNbItems) {
      throw std::runtime_error(
              cmd + " Unexpected number of items at line " + std::to_string(i) + ", got " + std::to_string(items.size())
              + ", " + std::to_string(expectedNbItems) + " expected");
    }
    handle(i,items);
  }

}

// Parse a time value ([0-9.]+)(s|ms|us|ns)
static double parse_time(string &value) {

  double ratio = 1.0;
  if(value.back()!='S')
    throw std::runtime_error(
            "hsm::parse_time() unexpected time value " + value);
  value.pop_back();

  switch (value.back()) {
    case 'M': // ms
      ratio = 1e-3;
      value.pop_back();
      break;
    case 'U': // us
      ratio = 1e-6;
      value.pop_back();
      break;
    case 'N': // ns
      ratio = 1e-9;
      value.pop_back();
      break;
  }

  return std::stod(value) * ratio;

}

// Parse HSM version number
static void parse_version(string& version,int32_t& major,int32_t& minor) {

  const std::regex version_number_regex("^([0-9]+)\\.([0-9]+)$");
  std::smatch regex_version_matches;

  if (not std::regex_match(version, regex_version_matches, version_number_regex)) {
    throw std::runtime_error("HSM version:" + version + " Unexpected format, major.minor expected");
  } else if (regex_version_matches.size() < 3) {
    throw std::runtime_error("HSM version:" + version + " Unexpected format, major.minor expected");
  } else {
    try {
      // regex_version_matches[0] is the full matched string
      major = (int32_t)std::stoi(regex_version_matches[1].str());
      minor = (int32_t)std::stoi(regex_version_matches[2].str());
    } catch (const std::invalid_argument &error) {
      throw std::runtime_error("HSM version:" + version + " Unexpected format, major.minor expected");
    }
  }

}

} // namespace hsm
