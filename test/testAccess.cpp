#include <tango.h>
#include <thread>

#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include "catch.hpp"
#include "TangoCatch.hpp"

using namespace std;

#define     SOSPARE_DEV_NAME    "test/ps-sospare/c01-1"

string TestedMagDevName = "test/m-of1/c01-b";
string TestedAccessDevName = "test/hsm/c01";

string ModbusSimDev1 = "test/ps-of1-modbus/c01-b";
string ModbusSimDev2 = "test/ps-sospare-modbus/c01-1";

//vector<string> TangoListener::devs = {TestedAccessDevName,TestedMagDevName,ModbusSimDev1,ModbusSimDev2};
vector<string> TangoListener::devs = {TestedAccessDevName,TestedMagDevName};

TEST_CASE( "Hot Swap Manager tango classes test", "tests" )
{
    Tango::DeviceProxy access_dev;
    Tango::DeviceProxy magnet;
    Tango::DeviceProxy converter;
    bool init_err = false;

    try
    {
        access_dev = Tango::DeviceProxy(TestedAccessDevName);
        access_dev.ping();

        magnet = Tango::DeviceProxy(TestedMagDevName);
        magnet.ping();

        converter = Tango::DeviceProxy("test/ps-of1/c01-b");
        converter.ping();
    }
    catch (Tango::DevFailed &e)
    {
        Tango::Except::print_exception(e);
        init_err = true;
    }

    REQUIRE_FALSE(init_err);

    SECTION("Socket state")
    {
        Tango::DeviceProxy sock("srmag/hsm_socket/c01");
        Tango::DevState sta = sock.state();
        REQUIRE(sta == Tango::ON);
        cout << "Socket state --> OK" << endl;
    }

    SECTION("Spare DC initial state")
    {
        Tango::DeviceProxy spare_dc(SOSPARE_DEV_NAME);
        Tango::DevState sta = spare_dc.state();
        REQUIRE(sta == Tango::STANDBY);
        cout << "Spare DC converter initial state --> OK" << endl;
    }

    SECTION("Magnet initial state")
    {
        Tango::DevState sta = magnet.state();
        REQUIRE(sta == Tango::ON);
        cout << "Magnet initial state --> OK" << endl;
    }

    SECTION("DC converter initial state")
    {
        Tango::DevState sta = converter.state();
        REQUIRE(sta == Tango::ON);
        cout << "DC converter initial state --> OK" << endl;
    }

    SECTION("At least one pipe data received")
    {
        vector<string> att_names = {"HsmName","Connected"};
        vector<Tango::DeviceAttribute> *atts = converter.read_attributes(att_names);
        string name;
        (*atts)[0] >> name;
        bool con;
        (*atts)[1] >> con;
        delete atts;

        REQUIRE(name == "of1b");
        REQUIRE(con == true);
        cout << "At least one pipe event received --> OK" << endl;
    }

    SECTION("Write/Read current on Magnet")
    {
        Tango::DeviceAttribute w_cur("Current",10.0);
        magnet.write_attribute(w_cur);

        this_thread::sleep_for(chrono::seconds(2));

        Tango::DeviceAttribute r_cur = magnet.read_attribute("Current");
        double read_current;
        r_cur >> read_current;
        REQUIRE(read_current == Approx(10.0).margin(0.5));

        Tango::DeviceAttribute r_cur_dc = converter.read_attribute("Current");
        double read_current_dc;
        r_cur_dc >> read_current_dc;
        REQUIRE(read_current_dc == Approx(10.0).margin(0.5));
        cout << "Write/Read current on magnet --> OK" << endl;
    }

    SECTION("Write/Read strength on Magnet")
    {
        Tango::DeviceAttribute w_cur("Strength",10.0);
        magnet.write_attribute(w_cur);

        this_thread::sleep_for(chrono::seconds(2));

        Tango::DeviceAttribute r_cur = magnet.read_attribute("Strength");
        double read_str;
        r_cur >> read_str;
        REQUIRE(read_str == Approx(10.0).margin(0.5));

        Tango::DeviceAttribute r_cur_dc = converter.read_attribute("Current");
        double read_current_dc;
        r_cur_dc >> read_current_dc;
        REQUIRE(read_current_dc == Approx(5.0).margin(0.5));
        cout << "Write/Read strength on magnet --> OK" << endl;
    }

    SECTION("Read possible Dc converter(s) for magnet")
    {
        Tango::AttributeInfoEx aie = magnet.get_attribute_config("DcConverter");
        REQUIRE(aie.enum_labels.size() == 2);
        REQUIRE(aie.enum_labels[0] == "of1b");
        REQUIRE(aie.enum_labels[1] == "sospare");

        Tango::DeviceAttribute da = magnet.read_attribute("DcConverter");
        Tango::DevShort tmp_sh;
        da >> tmp_sh;
        REQUIRE(aie.enum_labels[tmp_sh] == "of1b");
        cout << "Read possible DC for magnet --> OK" << endl;
    }

    SECTION("Read HSM mode")
    {
        Tango::AttributeInfoEx aie = access_dev.get_attribute_config("Mode");
        REQUIRE(aie.enum_labels.size() == 4);

        Tango::DeviceAttribute da = access_dev.read_attribute("Mode");
        Tango::DevShort tmp_sh;
        da >> tmp_sh;

        if (aie.enum_labels[tmp_sh] != "Master")
        {
            Tango::DeviceAttribute daw("Mode",(short)2);
            access_dev.write_attribute(daw);
        }

        da = access_dev.read_attribute("Mode");
        da >> tmp_sh;

        REQUIRE(aie.enum_labels[tmp_sh] == "Master");
        cout << "Read HSM mode --> OK" << endl;
    }

    SECTION("HSM mode to NOHSM")
    {
        Tango::DeviceAttribute daw("Mode",(short)0);
        access_dev.write_attribute(daw);

        Tango::DeviceAttribute da = access_dev.read_attribute("Mode");
        Tango::DevShort tmp_sh;
        da >> tmp_sh;

        REQUIRE(tmp_sh == 0);
        cout << "HSM mode set to NOHSM --> OK" << endl;
    }

    SECTION("Spare connected to magnet")
    {
        Tango::DeviceProxy spare_dc(SOSPARE_DEV_NAME);
        Tango::DeviceAttribute daw("ConnectedMagnetName",(short)2);
        spare_dc.write_attribute(daw);

        this_thread::sleep_for(chrono::seconds(2));

        Tango::AttributeInfoEx aie = spare_dc.get_attribute_config("ConnectedMagnetName");
        Tango::DeviceAttribute dar = spare_dc.read_attribute("ConnectedMagnetName");
        Tango::DevShort tmp_sh;
        dar >> tmp_sh;

        REQUIRE(aie.enum_labels[tmp_sh] == "of1b");
        cout << "Spare PS: Connected to magnet of1b --> OK" << endl;
    }

    SECTION("HSM mode to Master")
    {
        Tango::DeviceAttribute daw("Mode",(short)2);
        access_dev.write_attribute(daw);

        Tango::DeviceAttribute da = access_dev.read_attribute("Mode");
        Tango::DevShort tmp_sh;
        da >> tmp_sh;

        REQUIRE(tmp_sh == 2);
        cout << "HSM mode set to MASTER --> OK" << endl;
    }

    SECTION("Spare PS: Can't set connected magnet in Master mode")
    {
        Tango::DeviceProxy spare_dc(SOSPARE_DEV_NAME);
        Tango::DeviceAttribute da("ConnectedMagnetName","of1b");
        REQUIRE_THROWS(spare_dc.write_attribute(da));
        cout << "Spare PS: Can't set ConnectedMagnetName att in master mode --> OK" << endl;
    }

    SECTION("Spare PS: ConnectedMagnet none")
    {
        Tango::DeviceProxy spare_dc(SOSPARE_DEV_NAME);
        Tango::AttributeInfoEx aie = spare_dc.get_attribute_config("ConnectedMagnetName");

        Tango::DeviceAttribute da = spare_dc.read_attribute("ConnectedMagnetName");
        Tango::DevShort tmp_sh;
        da >> tmp_sh;

        REQUIRE(aie.enum_labels[tmp_sh] == "none");
        cout << "Spare PS: ConnectedMagnetName att is none --> OK" << endl;
    }

    SECTION("Swap Dc converter")
    {
        Tango::DeviceAttribute da("DcConverter",(short)1);
        magnet.write_attribute(da);

        this_thread::sleep_for(chrono::seconds(2));

        Tango::DeviceAttribute r_dc = magnet.read_attribute("DcConverter");
        Tango::DevShort sh;
        r_dc >> sh;
        REQUIRE(sh == 1);
        cout << "Swap Dc --> OK" << endl;
    }

    SECTION("After Swap")
    {
        Tango::DevState sta = magnet.state();
        REQUIRE(sta == Tango::ON);

        sta = converter.state();
        REQUIRE(sta == Tango::STANDBY);

        Tango::DeviceProxy spare_dc(SOSPARE_DEV_NAME);
        sta = spare_dc.state();
        REQUIRE(sta == Tango::ON);
        cout << "States after swap --> OK" << endl;
    }

    SECTION("Write/Read current on Magnet after Swap")
    {
        Tango::DeviceAttribute daw("Mode",(short)1);
        access_dev.write_attribute(daw);

        Tango::DeviceAttribute w_cur("Current",20.0);
        magnet.write_attribute(w_cur);

        this_thread::sleep_for(chrono::seconds(2));

        Tango::DeviceAttribute r_cur = magnet.read_attribute("Current");
        double read_current;
        r_cur >> read_current;
        REQUIRE(read_current == Approx(20.0).margin(0.5));

        Tango::DeviceProxy spare_dc(SOSPARE_DEV_NAME);
        Tango::DeviceAttribute r_cur_dc = spare_dc.read_attribute("Current");
        double read_current_dc;
        r_cur_dc >> read_current_dc;
        REQUIRE(read_current_dc == Approx(20.0).margin(0.5));
        cout << "Write/Read current on magnet after swap --> OK" << endl;
    }
}
