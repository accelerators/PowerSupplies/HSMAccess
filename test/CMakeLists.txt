cmake_minimum_required (VERSION 2.8) 
# MAKE_ENV is the path to find common environment to buil project 
# 
set(MAKE_ENV /segfs/tango/cppserver/env) 
# 
# Project definitions 
# 
project(TestHSMAccess) 
# 
# optional compiler flags 
# 
set(CXXFLAGS_USER -g) 
# 
# Get global information 
# 
include(${MAKE_ENV}/cmake_tango.opt) 
# 
# User additional include, link folders/libraries and source files 
# 
set(USER_INCL_DIR ) 
set(USER_LIB_DIR ) 
set(USER_LIBS ) 
set(USER_SRC_FILES ) 
# 
# Set gloabal info and include directories 
# 
set(SERVER_SRC testAccess.cpp) 
include_directories(${USER_INCL_DIR} ${TANGO_INCLUDES} /segfs/tango/tmp/manu/catch)
# 
# Device Server generation 
# 
set(SERVER_NAME test_HSMAccess) 
include(${MAKE_ENV}/cmake_common_target.opt) 
