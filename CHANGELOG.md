# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [11.6.0] - 07-06-2024
* add property currentInhibitionThreshold to forgotten inhibition on Sextupoles below a change delta

## [11.5.6] - 29-04-2024
* add memoried on 3 hotswap mode attribute
* fix status

## [11.5.5] - 12-04-2024
### Changed
* fix crash in lab condition certainly crash in real condition also.
* reduice timeout on libdeep to improved "Not able to acquire serialization"

## [11.5.4] - 21-03-2024
### Changed
* add check in saveCalibration do not recorded NaN values in Properties MostplateGain and MosplateOffset

## [11.5.3] - 11-01-2023
### Changed
* fix tipo

## [11.5.2] - 11-01-2023
### Changed
* add state of so/octupole mosplates hardware in the status

## [11.5.1] - 10-01-2023
### Changed
* fix state/status dlink error
* fix state/status blinking standby when Fault is present

## [11.5.0] - 11-12-2023
### Added
* Property SOSwapEnable possibility to configure if the mosplate is available and/or is Enable HOTSWAP for Sextupole/octupole

## [11.4.4] - 22-11-2023
### Fixed
* Persistence of disable state (noHsm)
* TriggerPosition when it is change by decimationRate way
### Changed
* set noHsm attribute to read only

## [11.4.3] - 17-11-2023
### Changed
* if disable do not send command to hsm (except reset)
* the way to become disable
* improved hard reboot cmd

## [11.4.2] - 27-10-2023
### Fixed
* Possibility to build ComecaChannel Server standalone (with Modbus)
### Changed
* change format %6.3 to %6.4 for Impedance Threshold (ComecaChannel)

## [11.4.1] - 25-10-2023
### Fixed
* fixed error "Instrument is in fault" to Fault State

## [11.4.0] - 20-10-2023
### Changed
* Authorized reset also in Disable state
* Keep thread and libdeep socket when NoHSM
* Convert error "Instrument is in fault" to Fault State
* changed state priority Now is Disable/Fault/Unknown/Alarm...
* add exception when Enable HSM is it is not possible

## [11.3.1] - 28-09-2023
### readded
* readded code deleted by pogo

## [11.3.0] - 28-09-2023

### added
* latched Alarm on new swapped magnets

## [11.2.1] - 26-09-2023

### Fixed
* do not exiting AcquisitionThread on error
* reload MOSPlateOffset form hsm Hardware without mA coefficient. (bug /1000 when "Save to DB")

## [11.2.0] - 14-09-2023

### Changed
* changed reset behaviour
### Added
* added HardReboot command

## [11.1.1] - 30-08-2023

### Changed
* reading of MOSPlateDataDate in good order
* init values
* offset +1 sampling values
* change labels

## [11.1.0] - 03-08-2023

### Changed
* reading of MOSPlateDataDate new data order

### Added
* TriggerPosition and DecimationRate attribute

## [11.0.10] - 27-07-2023

### Added
* attribute LastMostPlateDataRefresh

### Changed
* behavior MOSPlateDataDate now is rated by the hsm and 0 come from HSM

## [11.0.9] - 15-05-2023

### Changed
* move ModbusStat in ComecaChannel instead of HSMPowerSupply
* remove Tango StateMachine on ComecaChannel and HSMPowerSupply tango Class

## [10.0.1] - 12-10-2022

### Fixed
* **HSMPowerSupplyClass**: Remove the reset\_connected\_magnet\_name() call HSM
  MODE change [ACU-813](https://jira.esrf.fr/browse/ACU-813).

## [10.0.0] - 15-09-2022

### Changed
* **HSMAccess**: Refactor entierely the STATE/STATUS of the device. Now DISABLE
  = NoHSM, OFF = at least 1 magnet in PASSIVE, STDBY = at least 1 magnet in
  MASTER, ON = all magnets in HOTSWAP. ON switch to RUNNING when a swap
  occured. UNKNOWN/FAULT when something bad
  happens. [ACU-723](https://jira.esrf.fr/browse/ACU-723)

### Fixed
* **HSMAccess**: Reading MOSPlateData do no longer trigger the MOSSPY
  reading. [ACU-679](https://jira.esrf.fr/browse/ACU-679)
* **HSMAccess**: Event log now print timestamp in human readable format +
  Warning for C03 and C04. [ACU-743](https://jira.esrf.fr/browse/ACU-743)
* **HSMPowerSupply**: Inhibition is now allowed when in
  PASSIVE. [ACU-781](https://jira.esrf.fr/browse/ACU-781)
* **HSMPowerSupply**: Now send KP/TIs to the HSM when switching to
  MASTER. [ACU-782](https://jira.esrf.fr/browse/ACU-782)
* **HSMPowerSupply**: CURRENT and VOLTAGE attrs can now be written when in
  FAULT. [ACU-794](https://jira.esrf.fr/browse/ACU-794)


### Added
* **HSMAccess**: New command RefreshMOSPlateMonitoring that query the ?*MOSSPY,
  fill the MOSPlateData attribute and re-start the MONITORING
  afterwards. [ACU-679](https://jira.esrf.fr/browse/ACU-679)
* **HSMAccess**: New commands
  On/Off/Standby/Disable/Enable. [ACU-773](https://jira.esrf.fr/browse/ACU-773)

## [9.0.4] - 18-07-2022

### Fixed
* **HSMAccess**: HSM Current map now insert elements when updating the map, not
  when initializing the device.

## [9.0.3] - 11-07-2022

Added VPSPrefix class property used to compute magnet or vps device name.

## [9.0.2] - 27-06-2022

### Fixed
* **HSMAccess**: Modes attributes were not updated after a
  write. [ACU-753](https://jira.esrf.fr/browse/ACU-753)

## [9.0.1] - 13-06-2022

### Fixed
* **HSMAccess**: Revert CyclingOn and CyclingOff commands removal (used by
  Magnets, event if they are empty...)

## [9.0.0] - 03-06-2022

### Fixed
* **HSMAccess**: Now correctly update the Modes attributes by requesting them to
  the HSM when any mode attribute is read. [ACU-675](https://jira.esrf.fr/browse/ACU-675)
* **HSMPowerSupply**: Remove the Voltage/Current Tango setpoints being
  overwritten by the values queries to the HSM
  [ACU-641](https://jira.esrf.fr/browse/ACU-641)

### Changed
* **HSMPowerSupply**: HSM set point precision updated to %7.4f (was %6.3f)
* **HSMAccess**: EventLog now passively (no HSM queries) display the Event Log
  as a Spectrum of String, with the 1rst line being the timestamp of when the
  last refresh was perform [ACU-514](https://jira.esrf.fr/browse/ACU-514)
* **HSMPowerSupply**: FirmwareVersion renamed to DspAppVersion + New format
  (MAJOR_MINOR) [ACU-666](https://jira.esrf.fr/browse/ACU-666)
* **HSMPowerSupply**: For spares, the state switch to ALARM when swap occured
  (can be acknowledge with reset()) [ACU-718](https://jira.esrf.fr/browse/ACU-718)
* **HSMAccess**: reset() now call HardwareReset(FAULT) then HardwareReset(MMGR)
  [ACU-600](https://jira.esrf.fr/browse/ACU-600)

### Added
* **HSMAccess**: Add the MOSPLATE STATE monitoring coming from ?STATE MOS RAW
  and report-it to the device status/state as an error [ACU-515](https://jira.esrf.fr/browse/ACU-515).
* **HSMAccess**: New Commands RefreshLog & ClearLog [ACU-514](https://jira.esrf.fr/browse/ACU-514)
* **HSMAccess**: New Command HardwareReset(Str)
  [ACU-600](https://jira.esrf.fr/browse/ACU-600)
* **HSMAccess**: New Command "UnswapAll". [ACU-699](https://jira.esrf.fr/browse/ACU-699)

### Removed
* **HSMAccess**: CyclingOn and CyclingOff commands

## [8.0.0] - 24-02-2022

### Fixed
* **HSMPowerSupply**: Update the behavior concerning the inhibition
* **HSMAccess** call_magcfg: Use of regex to handle parsing more properly (i.e.: report unexpected data in logs warning ...). It fixes the HSMAccess not wroking properly since the add of the CFGSETTLE time in the MAGCFG.
* **HSMAccess** [ACU-514](https://jira.esrf.fr/browse/ACU-514) read_EventLog: Fix the event log read attribute
* **HSMPowerSupply** init_device: When parsing a string containing a version
  number, use regex instead of a priori index jumping in the string

### Added
* **CMake**: Add WITH\_MODBUS, WITH\_COMECACHANNEL, WITH\_COMECABOX,
  WITH\_LIBDEEP in order to change the default dependencies paths

### Changed
* **CMake**: Update the CMakeLists/Build strategy in order to isolate targets
  (Define OBJECT library...)
* **CMake**: Add ext/ComecaChannel submodule + Clean CMake for dependencies

## [7.1.2] - 03-05-2021

### Fixed
* Tango class **HSMAccess**: Before closing the connection to the HSM box,
add a wait time when writing the *NoHsm* attribute to true in order to be sure
that it is noticed by the acquisition thread

## [7.1.1] - 31-03-2021

### Changed
* Tango class **HSMPowerSupply**: Voltage thread. Do not send command to HSM if voltage
less than threshold (0.05 today)
* Tango class **HSMPowerSupply**: When a PS is swapped out, its state becomes
STANDBY

## [7.1.0] - 25-03-2021

### Changed
* Tango class **HSMPowerSupply**: Add a thread in its class object to send
magnet voltage to HSM

* Tango class **HSMPowerSupply**: Voltage and Current attributes INVALID in case of rack
error "not able to communicate"

* Tango class **HSMAccess**: When HSM is master or HotSwap, device state becomes
RUNNING in case of swapped magnet(s)

## [7.0.2] - 16-03-2021

### Changed
* Tango class **HSMPowerSupply**: Switch channel to FAULT state in case rack complains that it is not able to
communicate with it

* Tango class **HSMAccess**: HSM command MAGSET now returned the VMEAS  parameter.
Parsing adapted.

## [7.0.1] - 22-02-2021

### Fixed
* Tango class **HSMAccess**: Yet another bug related to cells 03 and 04 different cabling
schema

## [7.0.0] - 10-02-2021

### Added
* Tango class **HSMPowerSupply**: Add a new attribute for Modbus
statistics

## [6.2.1] - 10-02-2021 (never installed)

### Changed
* Tango class **HSMPowerSupply**: A new way to code changes due to cell 03
and 04. It is now coded as Antonin advices us.

## [6.2.0] - 21-01-2021

### Fixed
* Tango class **HSMPowerSupply**: Several bugs for management of cells 03 and
04 where cabling is different and HSM not modified

* Tango class **HSMPowerSupply**: Bug in CAN address computation for the
channel previously named sospare (name still valid for HSM) and now
named spare.

* Tango class **HSMPowerSupply**: Bug in spare channel ConnectedMagnetName
attribute when setting the enumeration labels

* Tango class **HSMPowerSupply**: HSM command MAGCURR knows only a fixed
cabling schema

* Tango class **HSMPowerSupply**: It is refused to connect the "spare" channel
on any sextupole or octupole

## [6.1.1] - 01-12-2020

### Added
* Tango class **ComecaChannel**: Change when building rack name. It take into account
more than 3 chars from device name (!)

## [6.1.0] - 18-11-2020

### Changed
* Tango class **HSMPowerSupply**: In status, still display channel status even if rack
is in FAULT state

## [6.0.1] - 20-10-22020

### Fixed
* Tango class **HSMAccess** : Fix bug in new attribute MagnetPSLink

## [6.0.0] - 15-10-2020

### Added
* Tango class **HSMAccess** : Add attribute *MagnetPSLink*

## [5.0.6] - 08-10-2020

### Fixed
* **ComecaChannel**: Fix bug in Impedance_I2 attribute
(still related to change in release 5.0.0...)

## [5.0.5] - 05-10-2020

### Fixed
* **HSMPowerSupply**: Better management of error while talking to
HSM box during "set_magnet()" method (device startup/reset)

## [5.0.4] - 02-10-2020

### Changed
* **HSMAccess**: Better re-connection to HSM after it has been switched
OFF/ON or reboot

### Fixed
* **HSMAccess**: Fix memory leak which could happen in case of failure
during HSM communication during init_device method (thanks Pablo)

## [5.0.3] - 30-09-2020

### Fixed
* **ComecaChannel**: Impedance attribute valid only if state is ON
or ALARM

## [5.0.2] - 29-09-2020

### Fixed
* **ComecaChannel**: Attribute Mode throws exception in case of
empty modbus data (due to missing rack)

## [5.0.1] - 29-09-2020

### Changed
* **HSMAccess** class: Remove the "CONF FACCTORY" HSM command during
DS startup sequence
* **HSMPowerSupply** class: When HSM mode changes to MASTER or HOTSWAP,
re-send current and voltage (to HSM)

### Fixed

* **ComecaChannel**: Forgot to change the modbus address when writing
impedance coeff to box (adr must be changed due to previous release change)

## [5.0.0] - 25-09-2020

### Changed
* **ComecaChannel**: Add new attribute *ImpedanceThreshold* which replaces
former *Impedance* attribute. Impedance attribute is now the impedance
computed by the hardware (read from Modbus interface)

## [4.1.0] - 10-08-2020

### Changed
* **ComecaChannel** and **HSMPowerSupply** classes: The impedance alarm is now latched in  hardware.
Reset command reset it !

## [4.0.3] - 20-07-2020

### Changed
* **HSMAccess**: Another change in "psName" for the old sospare

## [4.0.2] - 15-07-2020

### Changed
* **HSMAccess** class: Change in "psName" for spare PS (previous
sospare) in order to have a name coherent with HSM. Could be a temporary change
* **HSMAccess** class: Add HSM controller error message in device status

## [4.0.1] - 19-05-2020

### Changed
* **ComecaChannel** class: Change in alarm messages and remove one bit
of channels address dedicated to alarms as being one alarm.

## [4.0.0] - 11-05-2020

### Changed
* **HSMPowerSupply** class: Add management of MOSplate gain and offset via two
new properties. Send those values to HSM using the ADCCFFG command depending on
HSM firmware version

### Added
* **HSMAccess** class: New attribute FirmwareVersion

## [3.0.1] - 18-02-2020

### Fixed
* Bug crashing DS (!) when getting which magnet is connected to a spare
* Forgot the "none" case coming from HSM when a swapped in spare magnet is OFF

## [3.0.0] - 11-02-2020

### Added
* New attribute IsMOSPlateMonitoringOn
* For debugging: Add a ReadModbus command to read some modbus address

### Changed
* No automatic restart of MOS plate monitoring after reading its data
* Use a device buffer to store data returned when reading the MOSplate
(no allocation requiredto libdeep)
* Override the read_Current() and read_Voltage() methods in HSMPowerSupply Tango class
* Better management of the HSM "?state" command return value

## [2.4.0] - 17-12-2019

### Changed
* **HSMPowerSupply** class: Throw exception when trying to write current
which is a NaN value

## [2.3.3] - 10-12-2019

### Fixed
* **HSMPowerSupply** class: Protect code against pipe event received while the server is shutting
down
* **ComecaChannel** class: Protect code against empty modbus data
vector

## [2.3.2] - 14-11-2019

### Changed
* Archiving configured for Current attribute in **ComecaChannel**
class

## [2.3.1] - 29-10-2019

### Fixed
* Fix bug preventing status to correctly display message in case of wrong
property value for Kp, Ti or Z
* **ComecaChannel** class: Add security check on modbus data in
polled attributes code

## [2.3.0] - 25-10-2019

### Changed
* **ComecaChannel** class: Off command: If it is not executed after the request, do it another time
(workaround the Comeca firmware bug)
* **ComecaChannel** class: Read comeca channel modbus data even if device
in fault with acknowledge required in order to update the fault(s)
reported in status

## [2.2.0] - 11-10-2019

### Changed
* Reset command always allowed but do something only if device state
is FAULT
* **ComecaChannel** class: Off command allowed even in FAULT state (safety reason)

## [2.1.2] - 25-09-2019

### Fixed
* **ComecaChannel** class: Call Init command on Modbus device only in case of wrong modbus device
connection during init_device() method. Otherwise, due to limited
number of connection on Comeca box, we had error during the modbus
device

## [2.1.1] - 11-09-2019

### Fixed
* **ComecaChannel** class: Its always_executed_hook() method throws exception
in case of failure. Without this, in case of wrong modbus connection
(for instance due to other process eating the 4 avaiable connections),
the DS crashes

## [2.1.0] - 10-09-2019

### Added
* **ComecaChannel** class: Add a retry when the current is written (Comeca
soft bug workaround)

## [2.0.0] - 06-09-2019

### Added

* **HSMAccess** class: Add two new commands (CyclingOn and CyclingOff) to
manage HSM when one of its device is cycled.
**Warning**: Those two commands are today doing nothing!

## [1.4.0] - 18-07-2019

### Changed
* **ComecaChannel** class: Do not latch communication error like Timeout on
modbus. This problem will be debugged later on

## [1.3.0] - 16-07-2019

### Changed
* Change the way state is computed: Read rack state in always_executed_hook()
    and switch state to FAULT is rack is FAULT
* Read rack state at the end of init_device()
* Remove reading rack state in dev_state() because already read in always_executed_hook()

## [1.2.0] - 09-07-2019

### Fixed
* Bug with octu/sextu spare name changed from sospare to spare

### Added
* Some more debug messages

## [1.1.0] - 27-06-2019

### Added

* add management of cells 03 and 04 where there are on1 qf1 less but one
qf2 more (and the cabling is a little bit different)

## [1.0.2] - 26-06-2019

### Fixed

* Fix bug preventing DS to start when the Comeca boxes do not communicate at
all

## [1.0.1] - 07-06-2019

### Fixed

* Fix bug which prevented to do a reset when the rack was in FAULT.
The rror was that the Reset command is unavailable when the channel
is OFF (while it was FAULT)

## [1.0.0] - 04-06-2019

* First revision installed in EBS control system
