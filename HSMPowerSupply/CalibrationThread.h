//
// Calibration thread
//
#ifndef SCH_CALIBRATIONTHREAD_H
#define SCH_CALIBRATIONTHREAD_H

#include <HSMPowerSupply.h>

namespace HSMPowerSupply_ns {

class HSMPowerSupply;
class CalibrationThread : public omni_thread, public Tango::LogAdapter {

public:

  // Constructor
  CalibrationThread(HSMPowerSupply *,omni_mutex &,double ,double, double, double);
  void *run_undetached(void *);
  bool exitThread;
  void getStatus(Tango::DevState& state,string& status);

private:

  double lowCurrent;
  double highCurrent;
  double lowWait;
  double highWait;

  double lowHsmCurrent;
  double highHsmCurrent;

  omni_mutex &mutex;
  HSMPowerSupply *ds;
  string calibrationStatus;
  Tango::DevState state;

  void wait(double current,int ms);
  bool write_current(double current,int ms);
  bool set_calibration(double g,double o);

};

} // end namespace

#endif //SCH_CALIBRATIONTHREAD_H
