//
// CalibrationThread thread
//

#include "CalibrationThread.h"

namespace HSMPowerSupply_ns {

// ----------------------------------------------------------------------------------------
// Constructor
// ----------------------------------------------------------------------------------------
CalibrationThread::CalibrationThread(HSMPowerSupply *ds,omni_mutex &m,double lC,double lT,double hC,double hT):
        Tango::LogAdapter(ds), mutex(m), ds(ds), lowCurrent(lC), lowWait(lT), highCurrent(hC), highWait(hT) {

  exitThread = false;
  state = Tango::MOVING;
  mutex.lock();
  calibrationStatus = "Calibration in progress";
  mutex.unlock();
  start_undetached();

}

// ----------------------------------------------------------------------------------------
// Main loop
// ----------------------------------------------------------------------------------------
void *CalibrationThread::run_undetached(void *) {

  INFO_STREAM << "CalibrationThread::run(): Start thread" << endl;

  // Reset calibration
  if( !set_calibration(1.0,0.0) ) {
    return nullptr;
  }

  // Go to low current
  mutex.lock();
  calibrationStatus = "Calibration in progress: Waiting at low current";
  mutex.unlock();

  if(!write_current(lowCurrent,(int)(lowWait*1000.0))) {
    return nullptr;
  }

  lowHsmCurrent = ds->attr_HsmCurrent_read[0];

  // Go to high current
  mutex.lock();
  calibrationStatus = "Calibration in progress: Waiting at high current";
  mutex.unlock();

  if(!write_current(highCurrent,(int)(highWait*1000.0))) {
    return nullptr;
  }

  highHsmCurrent = ds->attr_HsmCurrent_read[0];

  // Compute calibration
  // We want that hsmCurrent and statumCurrent match for the 2 measurements, so we want that:
  // g*lowHsmCurrent + o = lowCurrent
  // g*highHsmCurrent + o = highCurrent

  // Solve system using Cramer's rule
  double _d = lowHsmCurrent - highHsmCurrent;
  double _dg = lowCurrent - highCurrent;
  double _do = lowHsmCurrent*highCurrent - highHsmCurrent*lowCurrent;
  double g = _dg / _d;
  double o = _do / _d;
  if( !set_calibration(g,o)) {
    return nullptr;
  }

  // Reset state
  mutex.lock();
  calibrationStatus = "";
  state = Tango::ON;
  mutex.unlock();
  exitThread = true;
  return nullptr;

}

// ----------------------------------------------------------------------------------------
// Write current,wait and check error
// ----------------------------------------------------------------------------------------
bool CalibrationThread::write_current(double current,int ms) {

  try {
    Tango::DeviceAttribute da("Current",current);
    ds->selfDS->write_attribute(da);
    try {
      wait(current, ms);
    } catch (Tango::DevFailed &e) {
      // Retry once (from time to time, current setpoint might be skipped by the underlying PS)
      ds->selfDS->write_attribute(da);
      wait(current, ms);
    }
  } catch (Tango::DevFailed &e) {
    mutex.lock();
    calibrationStatus = "Calibration failed: " + string(e.errors[0].desc);
    state = Tango::FAULT;
    mutex.unlock();
    exitThread = true;
    return false;
  }

  return !exitThread;

}

// ----------------------------------------------------------------------------------------
// apply calibration
// ----------------------------------------------------------------------------------------
bool CalibrationThread::set_calibration(double g,double o) {

  if( !HSMBase_ns::isGreater(ds->hsmMajor,ds->hsmMinor,2,4) ) {
    // Gain is the inverse (corrected in 2.4)
    g = 1.0/g;
  }

  try {
    ds->get_device_attr()->get_w_attr_by_name("MOSPlateGain").set_write_value(g);
    ds->get_device_attr()->get_w_attr_by_name("MOSPlateOffset").set_write_value(o*1000.0);
    ds->mOSPlateGain = g;
    ds->mOSPlateOffset = o*1000.0;
    vector<string> params = {ds->magName, to_string(g), to_string(o) + "A"};
    ds->hsm_command("WriteGainOffset", params);
  } catch (Tango::DevFailed &e) {
    mutex.lock();
    calibrationStatus = "Calibration failed: " + string(e.errors[0].desc);
    state = Tango::FAULT;
    mutex.unlock();
    exitThread = true;
    return false;
  }

  return true;

}

// ----------------------------------------------------------------------------------------
// Wait number of milliseconds
// ----------------------------------------------------------------------------------------
void CalibrationThread::wait(double current,int ms) {

  if(exitThread)
    return;

  int maxWaitRetry = 5;
  double delta = 2; // Amp
  double read_current;
  ds->selfDS->read_attribute("Current") >> read_current;
  while(!exitThread && maxWaitRetry>0 && fabs(current-read_current)>delta ) {

    // 10 sec max to reach current (5*2sec)
    maxWaitRetry--;
    int toSleep = 2000;
    while(toSleep>0 && !exitThread) {
      usleep(50000);
      toSleep -= 50;
    }
    if(!exitThread)
      ds->selfDS->read_attribute("Current") >> read_current;

  }

  if(maxWaitRetry==0) {
    Tango::Except::throw_exception(
            "CalibrationError",
            "Current (" + to_string(current) + " A) cannot be reached, (read=" + to_string(read_current) + ")",
            "CalibrationThread::wait"
            );
  }

  // Sleep at current setpoint
  int toSleep = ms;
  while (toSleep > 0 && !exitThread) {
    usleep(50000);
    toSleep -= 50;
  }

}

// ----------------------------------------------------------------------------------------
// Return status
// ----------------------------------------------------------------------------------------
void CalibrationThread::getStatus(Tango::DevState& state,string& status) {
  mutex.lock();
  status = calibrationStatus;
  state = this->state;
  mutex.unlock();
}

} // end namespace
