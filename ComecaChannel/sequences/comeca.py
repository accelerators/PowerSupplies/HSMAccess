"""Comeca PS test sequences

This module is imported at macroserver startup and on "ReloadMacroLib"

"""

from __future__ import print_function

import time
import numpy
from functools import wraps
from tango import DeviceProxy, DevState, DevFailed
import openpyxl as xl
import os

from pathlib2 import Path

from sardana.macroserver.macro import Macro
from sardana.macroserver.macro import Type

_VERSION = 1.5

#
# A decorator to retry function/method
#

def with_retry(fn):
    def wrapper(*args, **kargs):
        retry = 2
        while retry > 0:
            try:
                resp = fn(*args, **kargs)
                retry = 0
            except DevFailed as deverr:
                retry = retry - 1
                if retry == 0:
                    raise deverr
                else:
                    print("WARNING: --> DevFailed exception received :", deverr)
                    print("Retrying after some sleep time........")
                    time.sleep(0.5)
        return resp

    return wraps(fn)(wrapper)


#
# A small utility function involved in spreadsheet column size optimization
#

def as_text(value):
    return str(value) if value is not None else ""


# -------------------------------------------------------------------------------------
#
#                       Macro base class (common methods)
#
# -------------------------------------------------------------------------------------

class ComecaMacro(Macro):
    """Base class for Comeca PS test sequences macros
    Connects to several devices
    """
    keithley_i = DeviceProxy('test/k2701/curr')
    keithley_v = DeviceProxy('test/k2701/volt')
    lmg = DeviceProxy('test/lmg670/1')
    dc_ps = DeviceProxy('test/sm15k/1')

    box = DeviceProxy('test/ps-conv-rack/1')
    ch1 = DeviceProxy('test/ps-conv/1')
    ch2 = DeviceProxy('test/ps-conv/2')
    ch3 = DeviceProxy('test/ps-conv/3')
    channels = [ch1, ch2, ch3]

    lmg_ch1 = DeviceProxy('test/lmg670/ch1')
    lmg_ch2 = DeviceProxy('test/lmg670/ch2')
    lmg_ch3 = DeviceProxy('test/lmg670/ch3')
    lmg_ch4 = DeviceProxy('test/lmg670/ch4')

    keithley_i_admin = DeviceProxy(keithley_i.adm_name())
    keithley_v_admin = DeviceProxy(keithley_v.adm_name())
    lmg_admin = DeviceProxy(lmg.adm_name())
    admin_devs = [keithley_i_admin, keithley_v_admin, lmg_admin]

    def __init__(self, *args, **kwargs):
        super(ComecaMacro, self).__init__(*args,**kwargs)
        self.start_time = time.time()
        self.spent_time = 0
        self.lmg_6channels = False

    def macro_sleep(self, ti, full_time):
        """
        Our own sleep method (to be used if ti is a sec number)
        It allows the macro user to execute the door "StopMacro" command to
        stop the macro while it is in a sleep time
        """
        step = {}
        for _ in range(ti):
            time.sleep(1)
            self.spent_time = self.spent_time + 1
            st = round((self.spent_time / float(full_time)) * 100.0, 2)
            step['step'] = st
            self.checkPoint()
            yield step

    def setup_keithley(self, k_type):
        self.debug("Entering setup_keithley() for %s", k_type)
        if k_type == "I":
            dev = ComecaMacro.keithley_i
        else:
            dev = ComecaMacro.keithley_v

        k_state = dev.state()

        if k_state == DevState.FAULT:
            dev.reset()
            k_state = dev.state()

        if k_state == DevState.ON:
            meas_chan = dev.MeasurementChannel
            set_mc = False
            max_channel = 4

            if k_type == "I":
                if self.lmg_6channels is True:
                    max_channel = max_channel + 2   # 2 extra currents
                max_channel = max_channel + 4       # Temperatures

            for i in range(max_channel):
                if meas_chan[i] == False:
                    meas_chan[i] = True
                    set_mc = True
            if set_mc is True:
                dev.MeasurementChannel = meas_chan

    def setup_lmg(self):
        self.debug("Entering setup_lmg()")
        lmg_state = ComecaMacro.lmg.state()

        if lmg_state == DevState.FAULT:
            ComecaMacro.lmg.reset()

        if len(ComecaMacro.lmg.ChannelDevName) > 4:
            self.lmg_6channels = True

    def setup_dc(self):
        self.debug("Entering setup_dc()")
        dc_state = ComecaMacro.dc_ps.state()

        if dc_state == DevState.FAULT:
            ComecaMacro.dc_ps.reset()
            dc_state = ComecaMacro.dc_ps.state()

        if dc_state in (DevState.ON, DevState.OFF):
            ComecaMacro.dc_ps.Max_Power = 15000
            ComecaMacro.dc_ps.Max_Current = 30
            ComecaMacro.dc_ps.Voltage = 360

            if dc_state == DevState.OFF:
                ComecaMacro.dc_ps.On()

    def setup_comeca(self):
        self.debug("Entering setup_comeca()")
        box_state = ComecaMacro.box.state()

        if box_state in (DevState.FAULT, DevState.ALARM):
            ComecaMacro.box.reset()

        self.setup_comeca_channel(ComecaMacro.ch1)
        self.setup_comeca_channel(ComecaMacro.ch2)
        self.setup_comeca_channel(ComecaMacro.ch3)

    @staticmethod
    def stop_polling():
        try:
            kcur_cmd = ['test/k2701/curr', 'command', 'TakeMeasurement']
            kvol_cmd = ['test/k2701/volt', 'command', 'TakeMeasurement']
            ComecaMacro.keithley_i_admin.RemObjPolling(kcur_cmd)
            ComecaMacro.keithley_v_admin.RemObjPolling(kvol_cmd)
            lmg_cmd = ['test/lmg670/1', 'command', 'TakeMeasurement']
            ComecaMacro.lmg_admin.RemObjPolling(lmg_cmd)
        except DevFailed as deverr:
            print("WARNING: --> DevFailed exception received :", deverr)

    @staticmethod
    def start_polling():
        kcur_cmd = ([6000], ['test/k2701/curr', 'command', 'TakeMeasurement'])
        kvol_cmd = ([6000], ['test/k2701/volt', 'command', 'TakeMeasurement'])
        ComecaMacro.keithley_i_admin.AddObjPolling(kcur_cmd)
        ComecaMacro.keithley_v_admin.AddObjPolling(kvol_cmd)
        lmg_cmd = ([2000], ['test/lmg670/1', 'command', 'TakeMeasurement'])
        ComecaMacro.lmg_admin.AddObjPolling(lmg_cmd)

    @staticmethod
    def setup_comeca_channel(dev):
        ch1_state = dev.state()
        if ch1_state is DevState.ALARM:
            dev.Reset()
        ch1_state = dev.state()
        print("Sending OFF command to channel")
        if ch1_state is DevState.ON:
            dev.Off()

    def setup(self):
        """
        Set the test bench in a well defined state
        - Stop polling
        - Both Keithley to get 4 measurement channels (the first four)
        - LMG on
        - DC power supply ON with 360 Volt
        - All 3 comeca channels OFF
        """
        self.output("Entering setup() method")
        self.stop_polling()
        self.setup_lmg()
        self.setup_keithley("I")
        self.setup_keithley("V")
        self.setup_dc()
        self.setup_comeca()

    @with_retry
    def get_meta_data(self, usb, ser_num):
        """
        Get meta data and stored them in a dictionnary
        """
        self.debug("Entering get_meta_data() method")
#        di = {'when': time.strftime("%d/%m/%Y %H:%M:%S", time.localtime()),
#              'SerialNumber': ser_num, 'MacAddress': ComecaMacro.box.SerialNumber,
#              'IHM': ComecaMacro.box.FirmwareVersion,
#              'DSP': ComecaMacro.ch1.FirmwareVersion, 'USB': usb}
        di = {'when': time.strftime("%d/%m/%Y %H:%M:%S", time.localtime()),
              'SerialNumber': ser_num, 'MacAddress': ComecaMacro.box.MacAddress,
              'IHM': ComecaMacro.box.FirmwareVersion,
              'DSP': ComecaMacro.ch1.FirmwareVersion, 'USB': usb}
        return di

    @with_retry
    def all_channels_on(self):
        _ = [dev.On() for dev in ComecaMacro.channels]

    @with_retry
    def all_channels_off(self):
        self.output("Entering all_channels_off")
        _ = [dev.Off() for dev in ComecaMacro.channels]

    @with_retry
    def set_channels_current(self, *args):
        i = 0
        for dev in ComecaMacro.channels:
            if len(args) == 1:
                val = args[0]
            else:
                val = args[i]
            i = i + 1
            self.debug("Set channel %s current to %f A", dev.name(), val)
            dev.Current = val

    @staticmethod
    def check_channels_current(*args):
        i = 0
        for dev in ComecaMacro.channels:
            cur = dev.read_attribute("Current")
            if len(args) == 1:
                val = args[0]
            else:
                val = args[i]
            if abs(cur.value - val) > 2:
                str = "Channel {} do not generate the required current: {} instead of {}".format(dev.name(), cur.value, val)
                raise RuntimeError(str)
            i = i + 1

    @with_retry
    def set_channels_voltage(self, val):
        for dev in ComecaMacro.channels:
            self.debug("Set channel %s voltage to %f V", dev.name(), val)
            dev.Voltage = val

    @with_retry
    def dcps_off(self):
        dcps_state = ComecaMacro.dc_ps.state()
        if dcps_state is not DevState.OFF:
            ComecaMacro.dc_ps.Off()

    @staticmethod
    def in_hex(num):
        return '%#06x' % num

    @staticmethod
    def file_is_writable(filename):
        if filename[0] == '/':
            fname = filename + ".xlsx.tst"
        else:
            fname = "./" + filename + ".xlsx.tst"

        try:
            the_file = open(fname, "w")
            the_file.close()
            os.remove(fname)
            return True
        except IOError:
            return False

    def data_in_file(self, seq_name, meta, seq_meta, data, names, filename):
        """
        Save meta data, sequence meta data (if used) and data to the excel file in a single worksheet
        """
        self.output("Entering data_in_files() method")
        if filename[0] == '/':
            fname = filename + ".xlsx"
        else:
            fname = "./" + filename + ".xlsx"
        pa = Path(fname)
        if pa.is_file() == True:
            wb = xl.load_workbook(fname)
            ws = wb.create_sheet(seq_name)
        else:
            wb = xl.Workbook()
            ws = wb.active
            ws.title = seq_name
        self.meta_in_sheet(ws, meta)
        if seq_meta is not None:
            self.seq_meta_in_sheet(ws, seq_meta)
        self.data_in_sheet(ws, data, names)
        wb.save(fname)

    @staticmethod
    def meta_in_sheet(ws, meta):
        """
        Store the meta data in the work sheet
        """
        ws['A1'] = 'Date'
        ws['B1'] = meta['when']
        ws['B1'].alignment = xl.styles.Alignment(horizontal='right')

        ws['A2'] = "Rack serial number"
        ws['B2'] = meta['SerialNumber']
        ws['B2'].alignment = xl.styles.Alignment(horizontal='right')

        ws['A3'] = "Rack MAC address"
#        ws['B3'] = ComecaMacro.in_hex(meta['MacAddress'])
        ws['B3'] = meta['MacAddress']
        ws['B3'].alignment = xl.styles.Alignment(horizontal='right')

        ws['A4'] = "IHM board software version"
        ws['B4'] = meta['IHM']
        ws['B4'].alignment = xl.styles.Alignment(horizontal='right')

        ws['A5'] = "DSP boards software version"
        ws['B5'] = meta['DSP']
        ws['B5'].alignment = xl.styles.Alignment(horizontal='right')

        ws['A6'] = "Visu software version"
        ws['B6'] = meta['USB']
        ws['B6'].alignment = xl.styles.Alignment(horizontal='right')

    @staticmethod
    def seq_meta_in_sheet(ws, seq_meta):
        """
        Store the sequence meta data in the work sheet
        """
        ro = 8
        i = 0
        for v in seq_meta.iteritems():
            ws.cell(row=ro + i, column=1).value = v[0]
            val = v[1]
            ce = ws.cell(row=ro + i, column=2)
            if isinstance(val, basestring) is False:
                if isinstance(val, int) is False:
                    if val.is_integer() is False:
                        ce.number_format = '0.000'
            else:
                ce.alignment = xl.styles.Alignment(horizontal='right')
            ce.value = v[1]
            i = i + 1

    def data_in_sheet(self, ws, data, names):
        """
        Store the sequence data in the worksheet
        """
        self.debug("Entering data_in_sheet() method")
        min_r = 11

        j = 0
        for row in ws.iter_rows(min_row=min_r, max_row=min_r + len(names) - 1, max_col=1):
            row[0].value = names[j]
            j = j + 1

        for i in range(len(data)):
            j = 0
            for row in ws.iter_rows(min_row=min_r, max_row=min_r + len(data[i]) - 1, min_col=i + 2, max_col=i + 2):
                val = data[i][j]
                if isinstance(val, basestring) is False:
                    if isinstance(val, int) is False:
                        if val.is_integer() is False:
                            row[0].number_format = '0.000'
                else:
                    row[0].alignment = xl.styles.Alignment(horizontal='right')
                row[0].value = val
                j = j + 1

        for column_cells in ws.columns:
            length = max(len(as_text(cell.value)) for cell in column_cells)
            ws.column_dimensions[column_cells[0].column].width = length + 1

    @staticmethod
    def on_except():
        ComecaMacro.keithley_i.Trigger = 1  # Immediate triggering
        ComecaMacro.keithley_v.Trigger = 1  # Immediate triggering

    @staticmethod
    def check_channels_state():
        for dev in ComecaMacro.channels:
            dev_state = dev.state()
            if dev_state not in [DevState.ON, DevState.ALARM]:
                raise RuntimeError("At least one channel is not in ON (or ALARM) state")


# -------------------------------------------------------------------------------------
#
#                       CurrentLinearity sequence
#
# -------------------------------------------------------------------------------------


class CurrentLinearity(ComecaMacro):
    param_def = [["filename", Type.String, "CurrentLinearity", "Output file name"],
                 ["USBfirm_rel", Type.Integer, 0, "ESRF_Visu software release number"],
                 ["SerialNumber", Type.Integer, 0, "Rack serial number"]]

    def __init__(self, *args, **kwargs):
        super(CurrentLinearity, self).__init__(*args, **kwargs)
        self.stored_except = None
        self.data = []
        self.meta_data = {}
        self.filename = None
        self.full_time_sec = 0
        self.spent_time = 0
        self.lmg_channels = [ComecaMacro.lmg_ch1, ComecaMacro.lmg_ch2, ComecaMacro.lmg_ch3, ComecaMacro.lmg_ch4]
        self.previous_I = 0

    def prepare(self, filename, usb_rel, ser_num):
        self.setup()

    def run(self, filename, usb_rel, ser_num):
        if self.file_is_writable(filename) is False:
            str = 'Can\'t write in file {}'.format(filename)
            raise RuntimeError(str)
        self.filename = filename

        if self.lmg_6channels is True:
            self.lmg_channels.append(DeviceProxy('test/lmg670/ch5'))
            self.lmg_channels.append(DeviceProxy('test/lmg670/ch6'))

        min = 10
        max = 130
        step = 10
        nb_loop = (max - min) / step
        sleep_loop = (2 * 60) + 30
        full_time_sec = (nb_loop * sleep_loop) + (20 * 60)
        #        full_time_sec = 44

        try:
            self.meta_data = self.get_meta_data(usb_rel, ser_num)
            self.output("Meta data read")

            self.set_channels_voltage(33)
            self.output("Set current to 110 A")
            self.set_channels_current(110)
            self.all_channels_on()
            self.output("All channels are ON")

            self.output("Going to wait for 2 mins...")
            for i in self.macro_sleep(2 * 60, full_time_sec):
                yield i
            self.previous_I = 10.0
            self.get_record(self.data)
            self.previous_I = 110.0
            self.output("Going to wait for 18 mins...")
            for i in self.macro_sleep(18 * 60, full_time_sec):
                yield i

            # self.output("Set current to 11 A")
            # self.set_channels_current(11)
            # for i in self.macro_sleep(1 * 20,full_time_sec):
            #     yield i
            # self.get_record(self.data)

            for cur in range(min, max, step):
                self.output("Set current to %d A", cur)
                nb_except = 0
                cur_ok = False

                while cur_ok is False:
                    try:
                        self.set_channels_current(cur)
                        time.sleep(5)
                        self.check_channels_current(cur)
                        cur_ok = True
                    except (RuntimeError, DevFailed):
                        nb_except = nb_except + 1
                        if nb_except == 2:
                            raise
                        
                self.output("Going to wait for 2 mins and 25 sec...")
                for i in self.macro_sleep(sleep_loop - 5, full_time_sec):
                    yield i
                self.get_record(self.data)

        except DevFailed as deverr:
            print("DevFailed exception received :", deverr)
            self.stored_except = deverr
            self.on_except()
        except RuntimeError as run_ex:
            print("Runtime error received")
            self.stored_except = run_ex
            self.on_except()
        finally:
            self.on_finally()

    def on_abort(self):
        self.on_except()
        self.on_finally()

    @with_retry
    def get_record(self, data):
        """
        Get one record data. This means:
        - Keithley's in external triggering
        - Start Keithley's
        - Start LMG (which will trigger the Keithley's)
        - Get data from Keithley's and Lmg
        - Format data for storing in WS
        - Restore Keithley's trigger
        """
        self.check_channels_state()

        self.output("Getting one record")
        ComecaMacro.keithley_i.Trigger = 0  # External triggering
        ComecaMacro.keithley_v.Trigger = 0  # External triggering
        print("Keithley's in external triggering")

        ComecaMacro.keithley_i.takemeasurement()
        ComecaMacro.keithley_v.takemeasurement()

        when = time.strftime("%d/%m/%Y %H:%M:%S", time.localtime())
        dc_volt = ComecaMacro.dc_ps.read_attribute("Voltage")
        print("dc write value = ", dc_volt.w_value)
        chs = [dev.read_attributes(("Current", "Voltage", "Voltage_DC")) for dev in self.channels]
        print("Channel 1 current = ", chs[0][0].value)

        ComecaMacro.lmg.takemeasurement()
        time.sleep(10)  # Takes time for Keithley to read their input

        k_v = ComecaMacro.keithley_v.read_attributes(("V1", "V2", "V3", "V4"))
        print("Keithley V1 read = ", k_v[0].value)

        reading_ok = False
        for i in range(3):
            if self.lmg_6channels is True:
                k_i = ComecaMacro.keithley_i.read_attributes(("I1", "I2", "I3", "I4", "I5", "I6"))
            else:
                k_i = ComecaMacro.keithley_i.read_attributes(("I1", "I2", "I3", "I4"))
            print("Keithley I1 read = ", k_i[0].value)
            if abs(k_i[0].value - self.previous_I) > 1.0:
                reading_ok = True
                self.previous_I = k_i[0].value
                break
            else:
                print("Strange reading on I1 - Retrying it")
                ComecaMacro.keithley_i.Trigger = 1
                time.sleep(2)
                ComecaMacro.keithley_i.Trigger = 0
                ComecaMacro.keithley_i.takemeasurement()
                ComecaMacro.lmg.takemeasurement()
                time.sleep(10)

        if reading_ok is False:
            str = 'Strange reading from I keithley ({})'.format(k_i[0].value)
            raise RuntimeError(str)      

        t_i = ComecaMacro.keithley_i.read_attributes(("T1", "T2", "T3", "T4"))
        lmg_data = [dev.read_attributes(("Idc", "Udc")) for dev in self.lmg_channels]
        effs = ComecaMacro.lmg.read_attributes(("Efficiency1", "Efficiency2", "Efficiency3"))
        print("Efficiency ch1 = ", effs[0].value)

        print("Reset equipments to their default's")
        ComecaMacro.keithley_i.Trigger = 1  # Immediate triggering
        ComecaMacro.keithley_v.Trigger = 1  # Immediate triggering

        local_data = [when]
        local_data = local_data + [dc_volt.w_value, k_v[3].value, k_i[3].value]
        if self.lmg_6channels is True:
            local_data = local_data + [k_i[4].value, k_i[5].value]
        local_data = local_data + [lmg_data[3][1].value]
        if self.lmg_6channels is True:
			local_data = local_data + [lmg_data[3][0].value,lmg_data[4][0].value,lmg_data[5][0].value]
        else:
            local_data = local_data + [lmg_data[3][0].value]

        for i in range(4):
            local_data = local_data + [t_i[i].value]

        for i in range(3):
            ch_data = [chs[i][0].w_value, chs[i][0].value, chs[i][1].w_value, chs[i][1].value, chs[i][2].value,
                       k_i[i].value, k_v[i].value, lmg_data[i][0].value, lmg_data[i][1].value, effs[i].value]
            local_data = local_data + ch_data
        data.append(local_data)

    @staticmethod
    def add_ch_names(ch, names):
        names.append(ch + " Set current")
        names.append(ch + " Read current")
        names.append(ch + " Set voltage")
        names.append(ch + " Read voltage")
        names.append(ch + " DC voltage")
        names.append(ch + " Meas current (Keithley)")
        names.append(ch + " Meas voltage (Keithley)")
        names.append(ch + " Meas current (LMG)")
        names.append(ch + " Meas voltage (LMG)")
        names.append(ch + " Efficiency")

    def on_finally(self):
        self.all_channels_off()
        self.dcps_off()

        data_names = ["Time", "Set DC voltage", "Meas DC voltage (Keithley)"]
        if self.lmg_6channels is True:
            data_names = data_names + ["Ch1 Meas DC current (Keithley)"]
            data_names = data_names + ["Ch2 Meas DC current (Keithley)", "Ch3 Meas DC current (Keithley)"]
        else:
            data_names = data_names + ["meas DC current (Keithley)"]
        data_names = data_names + ["Meas DC voltage (LMG)"]
        if self.lmg_6channels is True:
            data_names = data_names + ["Meas DC current (LMG - ch1)","Meas DC current (LMG - ch2)","Meas DC current (LMG - ch3)"]
        else:
            data_names = data_names + ["Meas DC current (LMG)"]
        data_names = data_names + ["Temp1", "Temp2", "Temp3", "Temp4"]
        self.add_ch_names("Ch1", data_names)
        self.add_ch_names("Ch2", data_names)
        self.add_ch_names("Ch3", data_names)

        self.data_in_file("CurrentLinearity", self.meta_data, None, self.data, data_names, self.filename)

        self.start_polling()

        if self.stored_except is not None:
            raise self.stored_except


# -------------------------------------------------------------------------------------
#
#                       VoltageLinearity sequence
#
# -------------------------------------------------------------------------------------

class VoltageLinearity(ComecaMacro):
    param_def = [["filename", Type.String, "VoltageLinearity", "Output file name"],
                 ["USBfirm_rel", Type.Integer, 0, "ESRF_Visu software release number"],
                 ["SerialNumber", Type.Integer, 0, "Rack serial number"]]

    def __init__(self, *args, **kwargs):
        super(VoltageLinearity, self).__init__(*args, **kwargs)
        self.stored_except = None
        self.filename = None
        self.seq_meta_data = None
        self.meta_data = {}
        self.spent_time = 0
        self.data = []

    def prepare(self, filename, usb_rel, ser_num):
        self.setup()

    def run(self, filename, usb_rel, ser_num):
        if self.file_is_writable(filename) is False:
            str = 'Can\'t write in file {}'.format(filename)
            raise RuntimeError(str)
        self.filename = filename

        min = 3
        max = 36
        step = 3
        nb_loop = (max - min) / step
        sleep_loop = 25 + 2
        full_time_sec = (nb_loop * sleep_loop) + 2

        try:
            self.meta_data = self.get_meta_data(usb_rel, ser_num)
            self.output("Meta data read")

            self.set_channels_voltage(10)
            self.set_channels_current(10)
            self.all_channels_on()
            self.output("All channels are ON")
            for i in self.macro_sleep(2, full_time_sec):
                yield i

            self.seq_meta_data = self.get_seq_meta_data()

            for vol in range(min, max, step):
                self.output("Set channels voltage to %d V", vol)
                self.set_channels_voltage(vol)
                for i in self.macro_sleep(sleep_loop, full_time_sec):
                    yield i
                self.get_record(self.data)

        except DevFailed as deverr:
            print("DevFailed exception received :", deverr)
            self.stored_except = deverr
            self.on_except()
        except RuntimeError as run_ex:
            print("Runtime error received")
            self.stored_except = run_ex
            self.on_except()
        finally:
            self.on_finally()

    def on_abort(self):
        self.on_except()
        self.on_finally()

    @with_retry
    def get_seq_meta_data(self):
        """
        Get sequence meta data and stored them in a dictionary
        """
        self.debug("Entering get_seq_meta_data() method")
        di = {'Set DC voltage': ComecaMacro.dc_ps.read_attribute("Voltage").w_value,
              'Meas DC voltage (Keithley)': ComecaMacro.keithley_v.read_attribute('V4').value}
        return di

    @with_retry
    def get_record(self, data):
        """
        Get one record data. This means:
        - Keithley's in external triggering
        - Start Keithley's
        - Start LMG (which will trigger the Keithley's)
        - Get data from Keithley's and channels
        - Format data for storing in WS
        - Restore Keithley's trigger
        """
        self.check_channels_state()

        self.output("Getting one record")
        ComecaMacro.keithley_i.Trigger = 0  # External triggering
        ComecaMacro.keithley_v.Trigger = 0  # External triggering
        print("Keithley's in external triggering")

        ComecaMacro.keithley_v.takemeasurement()
        time.sleep(0.1)
        ComecaMacro.lmg.takemeasurement()
        time.sleep(10)  # Takes time for Keithley to read their input

        k_v = ComecaMacro.keithley_v.read_attributes(("V1", "V2", "V3", "V4"))
        chs = [dev.read_attribute("Voltage") for dev in ComecaMacro.channels]

        print("Reset equipments to their default's")
        ComecaMacro.keithley_i.Trigger = 1  # Immediate triggering
        ComecaMacro.keithley_v.Trigger = 1  # Immediate triggering

        local_data = []
        for i in range(3):
            ch_data = [chs[i].w_value, chs[i].value, k_v[i].value]
            local_data = local_data + ch_data
        data.append(local_data)

    @staticmethod
    def add_ch_names(ch, names):
        names.append(ch + " Set voltage")
        names.append(ch + " Read voltage")
        names.append(ch + " Meas voltage (Keithley)")

    def on_finally(self):
        self.all_channels_off()
        self.dcps_off()

        data_names = []
        self.add_ch_names("Ch1", data_names)
        self.add_ch_names("Ch2", data_names)
        self.add_ch_names("Ch3", data_names)

        self.data_in_file("VoltageLinearity", self.meta_data, self.seq_meta_data, self.data, data_names, self.filename)

        self.start_polling()

        if self.stored_except is not None:
            raise self.stored_except


# -------------------------------------------------------------------------------------
#
#                       DcInputChange sequence
#
# -------------------------------------------------------------------------------------

class DcInputChange(ComecaMacro):
    param_def = [["Ch1 current", Type.Float, 10.0, "Channel 1 current"],
                 ["Ch2 current", Type.Float, 10.0, "Channel 2 current"],
                 ["Ch3 current", Type.Float, 10.0, "Channel 3 current"],
                 ["filename", Type.String, "DcInputChange", "Output file name"],
                 ["USBfirm_rel", Type.Integer, 0, "ESRF_Visu software release number"],
                 ["SerialNumber", Type.Integer, 0, "Rack serial number"]]

    def __init__(self, *args, **kwargs):
        super(DcInputChange, self).__init__(*args, **kwargs)
        self.data = []
        self.filename = None
        self.meta_data = {}
        self.seq_meta_data = None
        self.stored_except = None
        self.spent_time = 0

    def prepare(self, ch1_i, ch2_i, ch3_i, filename, usb_rel, ser_num):
        self.setup()

    def run(self, ch1_i, ch2_i, ch3_i, filename, usb_rel, ser_num):
        if self.file_is_writable(filename) is False:
            str = 'Can\'t write in file {}'.format(filename)
            raise RuntimeError(str)
        self.filename = filename

        full_time_sec = 5 + (10 * 4) + (4 * 2)

        try:
            self.meta_data = self.get_meta_data(usb_rel, ser_num)
            self.output("Meta data read")

            self.set_channels_voltage(33)
            self.set_channels_current(ch1_i, ch2_i, ch3_i)
            self.all_channels_on()
            self.output("All channels are ON")
            for i in self.macro_sleep(5, full_time_sec):  # Give time to rack to set current
                yield i
            self.check_channels_current(ch1_i, ch2_i, ch3_i)

            self.seq_meta_data = self.get_seq_meta_data()

            for i in self.one_point(360, full_time_sec):
                yield i
            for i in self.one_point(341, full_time_sec):
                yield i
            for i in self.one_point(379, full_time_sec):
                yield i
            for i in self.one_point(360, full_time_sec):
                yield i

        except DevFailed as deverr:
            print("DevFailed exception received :", deverr)
            self.stored_except = deverr
            self.on_except()
        except RuntimeError as run_ex:
            print("Runtime error received")
            self.stored_except = run_ex
            self.on_except()
        finally:
            self.on_finally()

    def on_abort(self):
        self.on_except()
        self.on_finally()

    def one_point(self, volt, full_time):
        """
        Set DC voltage, wait for a while and get data
        """
        self.output("Entering one_point() for voltage %d V", volt)
        ComecaMacro.dc_ps.Voltage = volt
        for i in self.macro_sleep(5, full_time):
            yield i
        self.get_record(self.data)

    @with_retry
    def get_seq_meta_data(self):
        """
        Get sequence meta data and stored them in a dictionary
        """
        self.debug("Entering get_seq_meta_data() method")
        di = {'Ch1 current': ComecaMacro.ch1.read_attribute("Current").w_value,
              'Ch2 current': ComecaMacro.ch2.read_attribute("Current").w_value,
              'Ch3 current': ComecaMacro.ch3.read_attribute("Current").w_value}
        return di

    @with_retry
    def get_record(self, data):
        """
        Get one record data. This means:
        - Keithley's in external triggering
        - Start Keithley's
        - Start LMG (which will trigger the Keithley's)
        - Get data from Keithley's
        - Format data for storing in WS
        - Restore Keithley's trigger
        """
        self.check_channels_state()

        self.output("Getting one record")
        ComecaMacro.keithley_i.Trigger = 0  # External triggering
        ComecaMacro.keithley_v.Trigger = 0  # External triggering
        print("Keithley's in external triggering")

        dc_v = ComecaMacro.dc_ps.read_attribute("Voltage").w_value
        ComecaMacro.keithley_i.takemeasurement()
        ComecaMacro.keithley_v.takemeasurement()
        print("keithley i status %s", ComecaMacro.keithley_i.status())
        print("keithley v status %s", ComecaMacro.keithley_v.status())
        time.sleep(0.1)
        ComecaMacro.lmg.takemeasurement()
        time.sleep(10)  # Takes time for Keithley to read their input

        if self.lmg_6channels is True:
            k_i = ComecaMacro.keithley_i.read_attributes(("I1", "I2", "I3", "I4", "I5", "I6"))
        else:
            k_i = ComecaMacro.keithley_i.read_attributes(("I1", "I2", "I3", "I4"))
        k_v = ComecaMacro.keithley_v.read_attribute("V4")

        print("Reset equipments to their default's")
        ComecaMacro.keithley_i.Trigger = 1  # Immediate triggering
        ComecaMacro.keithley_v.Trigger = 1  # Immediate triggering

        local_data = [dc_v, k_v.value]
        if self.lmg_6channels is True:
            local_data = local_data + [k_i[3].value + k_i[4].value + k_i[5].value]
        else:
            local_data = local_data + [k_i[3].value]
        local_data = local_data + [k_i[0].value, k_i[1].value, k_i[2].value]
        data.append(local_data)

    def on_finally(self):
        self.all_channels_off()
        self.dcps_off()

        data_names = ["Set DC voltage", "Meas DC voltage (Keithley)"]
        if self.lmg_6channels is True:
            data_names = data_names + ["Meas DC current (Keithley 4+5+6)"]
        else:
            data_names = data_names + ["Meas DC current (Keithley)"]
        data_names = data_names + ["Ch1 Meas current (Keithley)", "Ch2 Meas current (keithley)",
                                   "Ch3 Meas current (Keithley)"]

        self.data_in_file("DCInputChange", self.meta_data, self.seq_meta_data, self.data, data_names, self.filename)

        self.start_polling()

        if self.stored_except is not None:
            raise self.stored_except
